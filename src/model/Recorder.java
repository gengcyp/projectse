package model;

import java.util.ArrayList;

import controller.CategoryController;
import controller.RecordController;

public class Recorder {
	private ArrayList<Record> records;
	private ArrayList<Category> categories;
	private CategoryController cController;
	private RecordController rController;
	
	public Recorder() {
		records = new ArrayList<>();
		categories = new ArrayList<>();
		cController = new CategoryController();
		rController = new RecordController();
	}
}
