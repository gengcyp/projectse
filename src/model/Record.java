package model;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

public class Record {
	private Hashtable<String, String> detail;
	private Set<String> header;
	private Category category;
	private ArrayList<Tag> tags;

	public Record(Category c) {
		category = c;
		header = category.getHeader2().keySet();
	}
	
	public boolean addDetail(String key, String value){
		if (header.contains(key)){
			detail.put(key, value);
			return true;
		}
		return false;
	}
	
	
	public Set<String> getHeader() {
		return header;
	}
	public void setHeader(Set<String> header) {
		this.header = header;
	}
	public Hashtable<String, String> getDetail() {
		return detail;
	}
	public void setDetail(Hashtable<String, String> detail) {
		this.detail = detail;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public ArrayList<Tag> getTags() {
		return tags;
	}
	public void setTags(ArrayList<Tag> tags) {
		this.tags = tags;
	}
}
