package model;

import java.util.ArrayList;

public class Tag {
	private String name;
	private ArrayList<Record> records;
	
	public Tag(String name) {
		this.name = name;
		records = new ArrayList<>();
	}
	
	public void addRecord(Record rec){
		records.add(rec);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Record> getRecords() {
		return records;
	}
	public void setRecords(ArrayList<Record> records) {
		this.records = records;
	}
}
