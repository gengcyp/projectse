package model;

import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ModelTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Category c = new Category("A");
		c.addHeader("Name", new JTextField());
		c.addHeader("Short Story", new JTextArea());
		c.addHeader("Status", new JComboBox<>());
		System.out.println(c.getHeader2().keySet());
	}

}
