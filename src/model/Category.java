package model;

import java.awt.Component;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Category {
	private String name;
	private Set<String> header;
	private HashMap<String, Component> header2;

	public Category(String name) {
		this.name = name;
		header = new HashSet<>();
		header2 = new HashMap<>();
	}
	
	public void addHeader(String head, Component c){
		header.add(head);
		header2.put(head, c);
	}
	
	public boolean removeHeader(String head){
		header2.remove(head);
		return header.remove(head);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<String> getHeader() {
		return header;
	}
	public void setHeader(Set<String> header) {
		this.header = header;
	}
	public HashMap<String, Component> getHeader2() {
		return header2;
	}
	public void setHeader2(HashMap<String, Component> header2) {
		this.header2 = header2;
	}

}
